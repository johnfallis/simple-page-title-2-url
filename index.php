<?php
require 'pageTitle2Url.php';

$pageTitle = 'Fifa arrests: Vladimir Putin backs Sepp Blatter and slams US "meddling"';
$pageTitle2Url = new pageTitle2Url();

try {
    $exampleOne = $pageTitle2Url->setPageTitle($pageTitle)
                    ->setDefaultPatterns()->getUrl();
} catch (Exception $e) {
    echo $e->getMessage();
}
try {
    $exampleTwo = $pageTitle2Url->setRegEx('/Vladimir Putin/', 'Grey Cardinal', true)->getUrl();
} catch (Exception $e) {
    echo $e->getMessage();
}
?>
<h2>Example One </h2>
<code>
    <pre>
try {
    $pageTitle = 'Fifa arrests: Vladimir Putin backs Sepp Blatter and slams US "meddling"';
    $pageTitle2Url = new pageTitle2Url();
    echo $pageTitle2Url->setPageTitle($pageTitle)->setDefaultPatterns()->getUrl();
} catch (Exception $e) {
    echo $e->getMessage();
}
    </pre>
</code>
<h3>Sample </h3>
<?= $exampleOne ?>
<h2>Example Two </h2>
<code>
    <pre>
try {
    $pageTitle = 'Fifa arrests: Vladimir Putin backs Sepp Blatter and slams US "meddling"';
    $pageTitle2Url = new pageTitle2Url();
    echo $pageTitle2Url->setPageTitle($pageTitle)->setDefaultPatterns()->setRegEx('/Vladimir Putin/', 'Grey Cardinal', true)->getUrl();
} catch (Exception $e) {
    echo $e->getMessage();
}
    </pre>
</code>
<h3>Sample </h3>
<?= $exampleTwo ?>
