<?php

namespace Vendor\OrganicWebsite\Framework;

class Autoloader {

    const DEFAULT_EXTENSION = '.php';

    private static $extension;
    private static $path;

    protected static function directoryExists($path) {
        return \is_dir($path) && \file_exists($path);
    }

    protected static function setRootDirectory($path) {
        if (!self::directoryExists($path)) {
            throw new \InvalidArgumentException('Autoloader cannot find directory path: ' . $path);
        }
        self::$path = $path;
    }

    protected static function getRootDirectory() {
        return self::$path;
    }

    protected static function setIncludePath() {
        if (self::getRootDirectory() === null) {
            throw new \LogicException('Autoloader include directory not set');
        }
        set_include_path(get_include_path() . PATH_SEPARATOR . self::getRootDirectory());
    }

    protected static function getIncludePath() {
        return get_include_path();
    }

    protected static function setAutoloadFileExtension($extension) {
        if (\is_dir($extension)) {
            throw new \InvalidArgumentException('Autoloader requires a valid file extension: ' . $extension);
        }
        spl_autoload_extensions($extension);
        self::$extension = $extension;
    }

    protected static function getAutoloadFileExtension() {
        return self::$extension;
    }

    protected static function tearDown() {
        self::$extension = self::$path = null;
    }

    public static function hierarchy($path, $fileExtension = self::DEFAULT_EXTENSION) {
        self::setRootDirectory($path);
        self::setIncludePath();
        self::setAutoloadFileExtension($fileExtension);
        spl_autoload_register('spl_autoload');
        self::tearDown();
        return true;
    }

}

