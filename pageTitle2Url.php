<?php

class pageTitle2Url {

    // Remove everything except A-Z characters and spaces
    const PATTERN_AZ_CHAR = '/[^a-zA-Z0-9\s]/';
    const REPLACE_AZ_CHAR = '';
    // Remove duplicate spaces
    const PATTERN_DP_SPACE = '/\s+/';
    const REPLACE_DP_SPACE = ' ';
    // Hyphenate spaces
    const PATTERN_HP_SPACE = '/\s/';
    const REPLACE_HP_SPACE = '-';

    private $pageTitle;
    private $patterns;
    private $replacements;

    public function setPageTitle($title) {
        if ($title === '') {
            throw new InvalidArgumentException('Page title missing');
        }
        $this->pageTitle = $title;
        return $this;
    }

    public function setRegEx($pattern, $replacement, $prepend = false) {
        // error control operator misused
        if (@preg_match($pattern, null) === false || !is_string($replacement)) {
            throw new InvalidArgumentException('Invalid arguments');
        }
        switch ($prepend) {
            case true:
                array_unshift($this->patterns, $pattern);        
                array_unshift($this->replacements, $replacement);        
                break;
            default:
                $this->patterns[] = $pattern;
                $this->replacements[] = $replacement;
        }
        return $this;
    }

    public function setDefaultPatterns() {
        $this->setRegEx(self::PATTERN_AZ_CHAR, self::REPLACE_AZ_CHAR);
        $this->setRegEx(self::PATTERN_DP_SPACE, self::REPLACE_DP_SPACE);
        $this->setRegEx(self::PATTERN_HP_SPACE, self::REPLACE_HP_SPACE);
        return $this;
    }

    public function getUrl() {
        $title = $this->getPageTitle();
        $prettyUrl = preg_replace($this->getPatterns(), $this->getReplacements(), $title);
        return strtolower($prettyUrl);
    }

    protected function isPageTitle() {
        return $this->pageTitle !== null ? true : false;
    }

    protected function getPageTitle() {
        if ($this->isPageTitle() === false) {
            throw new Exception('Page title not set');
        }
        return $this->pageTitle;
    }

    protected function getPatterns() {
        return $this->patterns;
    }

    protected function getReplacements() {
        return $this->replacements;
    }

}
