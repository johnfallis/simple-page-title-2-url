<?php

require '../pageTitle2Url.php';

class pageTitle2UrlTest extends PHPUnit_Framework_TestCase {

    /**
     * @var pageTitle2Url
     */
    protected $object;

    protected function setUp() {
        $this->object = new pageTitle2Url;
    }

    /**
     * @covers pageTitle2Url::setPageTitle
     * @expectedException        InvalidArgumentException
     * @expectedExceptionMessage Page title missing
     */
    public function testSetPageTitleException() {
        $this->object->setPageTitle('');
    }

    /**
     * @covers pageTitle2Url::setPageTitle
     */
    public function testSetPageTitle() {
        $this->object->setPageTitle('Fifa arrests: Vladimir Putin backs Sepp Blatter and slams US "meddling"');
    }

    /**
     * @covers pageTitle2Url::setRegEx
     * @expectedException        InvalidArgumentException
     * @expectedExceptionMessage Invalid arguments
     */
    public function testSetRegExException() {
        // Remove the following lines when you implement this test.
        $this->object->setRegEx('/[/', 'something');
    }

    /**
     * @covers pageTitle2Url::setRegEx
     */
    public function testSetRegEx() {
        $this->object->setRegEx('/(fifa)/i', 'afif');
    }

    /**
     * @covers pageTitle2Url::setDefaultPatterns
     */
    public function testSetDefaultPatterns() {
        $this->object->setDefaultPatterns();
    }

    /**
     * @covers pageTitle2Url::getUrl
     */
    public function testGetUrl() {
        $pageTitle = 'Six astonishing predictions about the internet in 2019';
        $pageTitle2Url = new pageTitle2Url();
        $url = $pageTitle2Url->setPageTitle($pageTitle)->setDefaultPatterns()->getUrl();
        $this->assertEquals('six-astonishing-predictions-about-the-internet-in-2019', $url);
    }

}
